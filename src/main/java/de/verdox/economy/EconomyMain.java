package de.verdox.economy;

import de.verdox.economy.eco.EconomyImpl;
import de.verdox.economy.eco.commands.EconomyCommandExecutor;
import de.verdox.economy.eco.listener.EconomyListener;
import de.verdox.economy.eco.repository.DataConnectionImpl;
import de.verdox.economy.utils.UpdateChecker;
import de.verdox.economy.utils.configuration.ConfigHandler;
import net.milkbowl.vault.economy.Economy;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

public class EconomyMain extends JavaPlugin {
    private static int pluginID = 6518;
    public static EconomyImpl economy;
    public static EconomyMain plugin;
    public static DataConnectionImpl dataConnection;
    private static int SpigotID = 75246;

    @Override
    public void onDisable() {
        consoleMessage("&4Plugin disabled");
    }

    @Override
    public void onEnable() {
        plugin = this;

        Bukkit.getOnlinePlayers().forEach(s -> System.out.println(s.getUniqueId()));

        ConfigHandler.createConfigFile();
        if (!setupEconomy() ) {
            consoleMessage("&4Disabled due to no Vault dependency!");
            getServer().getPluginManager().disablePlugin(this);
        }
        dataConnection = new DataConnectionImpl(this);
        EconomyCommandExecutor economyCommandExecutor = new EconomyCommandExecutor();
        //Command registration
        this.getCommand("money").setExecutor(economyCommandExecutor);
        this.getCommand("pay").setExecutor(economyCommandExecutor);
        this.getCommand("baltop").setExecutor(economyCommandExecutor);
        this.getCommand("eco").setExecutor(economyCommandExecutor);
        this.getCommand("reloadeconomy").setExecutor(economyCommandExecutor);
        this.getCommand("economyimport").setExecutor(economyCommandExecutor);

        //Event registration
        this.getServer().getPluginManager().registerEvents(new EconomyListener(),this);
        consoleMessage("&aPlugin loaded");
        Metrics metrics = new Metrics(this,pluginID);

        new UpdateChecker(this, SpigotID).getVersion(version -> {
            if (this.getDescription().getVersion().equalsIgnoreCase(version)) {
                consoleMessage("&aThere is no new Update available.");
            } else {
                    consoleMessage("&eThere is a new Update available. ");
                    consoleMessage("&eCurrent Version&7: &c"+this.getDescription().getVersion());
                    consoleMessage("&eLatest Version&7: &b"+version);
                    consoleMessage("");
                    consoleMessage("&4The Plugin Website has changed! &cPlease check this link");
                    consoleMessage("");
                    consoleMessage("&eDownload at: &bhttps://www.spigotmc.org/resources/v-economy-reborn-best-economy-solution-1-8-1-15.75246/");
            }
        });
    }

    public static void consoleMessage(String message){
        Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.translateAlternateColorCodes('&',"&8[&eV-Economy&8] "+message));
    }

    private boolean setupEconomy() {
        this.economy = new EconomyImpl();
        getServer().getServicesManager().register(Economy.class,economy,this, ServicePriority.High);
        return economy != null;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(!(sender instanceof Player)){
            EconomyMain.consoleMessage("&4Something tried to execute a command!");
            return true;
        }
        return false;
    }
}
