package de.verdox.economy.converter;

import de.verdox.economy.EconomyMain;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class GemsEconomyConverter implements Converter {
    @Override
    public boolean ymlConvert(boolean overwrite) {
        Path vEconomyFolder = FileSystems.getDefault().getPath(EconomyMain.plugin.getDataFolder().getAbsolutePath());
        Path pluginFolder = vEconomyFolder.getParent();
        Path userData = pluginFolder.getFileSystem().getPath(pluginFolder.toAbsolutePath()+"/GemsEconomy");

        EconomyMain.consoleMessage("&4Conversion started with overwrite = &b"+overwrite);

        if(!userData.toFile().exists() || !userData.toFile().isDirectory()){
            EconomyMain.consoleMessage("&4Conversion failed due to now GemsEconomy folder found!");
            return false;
        }

        File[] files = userData.toFile().listFiles();
        for(int i = 0;i<files.length;i++){
            if(files[i].getName().equals("data.yml")){
                YamlConfiguration data = YamlConfiguration.loadConfiguration(files[i]);
                ConfigurationSection accountSection = data.getConfigurationSection("accounts");
                ConfigurationSection currencies = data.getConfigurationSection("currencies");
                UUID currencyUUID = null;

                for(String key:currencies.getKeys(false)){
                    ConfigurationSection currency = currencies.getConfigurationSection(key);
                    if(currency.getBoolean("defaultcurrency")){
                        currencyUUID = UUID.fromString(key);
                        EconomyMain.consoleMessage("&4Found default Currency with uuid&7: &e"+currencyUUID);
                        break;
                    }
                }

                for(String key:accountSection.getKeys(false)){
                    ConfigurationSection userSection = accountSection.getConfigurationSection(key);
                    UUID uuid = UUID.fromString(userSection.getString("uuid"));
                    OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(uuid);
                    if(offlinePlayer == null)
                        continue;
                    if(!userSection.isSet("balances") || !userSection.isConfigurationSection("balances"))
                        continue;
                    ConfigurationSection balancesSection = userSection.getConfigurationSection("balances");
                    if(!balancesSection.isDouble(currencyUUID.toString()))
                        continue;

                    Double money = balancesSection.getDouble(currencyUUID.toString());

                    if (overwrite == false && EconomyMain.economy.hasAccount(offlinePlayer)) {
                        EconomyMain.consoleMessage("&e"+offlinePlayer.getName()+" &calready has an account. Skipping! &7[Overwrite = false!]");
                    }
                    else if(overwrite == true && EconomyMain.economy.hasAccount(offlinePlayer)){
                        EconomyMain.economy.withdrawPlayer(offlinePlayer,EconomyMain.economy.getBalance(offlinePlayer));
                        EconomyMain.economy.depositPlayer(offlinePlayer,money);
                        EconomyMain.consoleMessage("&e"+offlinePlayer.getName()+" &aconverted successfully with Balance: &b"+money);
                    }
                    else{
                        EconomyMain.economy.createPlayerAccount(offlinePlayer);
                        EconomyMain.economy.depositPlayer(offlinePlayer,money);
                        EconomyMain.consoleMessage("&e"+offlinePlayer.getName()+" &aconverted successfully with Balance: &b"+money);
                    }
                }
            }
        }

        return false;
    }

    @Override
    public boolean sqLiteConvert(boolean overwrite) throws SQLException {
        Connection connection = null;
        Path vEconomyFolder = FileSystems.getDefault().getPath(EconomyMain.plugin.getDataFolder().getAbsolutePath());
        Path pluginFolder = vEconomyFolder.getParent();
        Path userData = pluginFolder.getFileSystem().getPath(pluginFolder.toAbsolutePath()+"/GemsEconomy");

        EconomyMain.consoleMessage("&4Conversion started with overwrite = &b"+overwrite);

        if(!userData.toFile().exists() || !userData.toFile().isDirectory()){
            EconomyMain.consoleMessage("&4Conversion failed due to now GemsEconomy folder found!");
            return false;
        }

        File[] files = userData.toFile().listFiles();
        for(int i = 0;i<files.length;i++){
            switch (files[i].getName()){
                case "database.sqlite":
                    if(connection!=null && !connection.isClosed()){
                        break;
                    }
                    connection = DriverManager.getConnection("jdbc:sqlite:"+files[i].getAbsolutePath());
                    EconomyMain.consoleMessage("&eConnected to SQLite file at: "+files[i].getAbsolutePath());
                    ResultSet result = connection.createStatement().executeQuery("SELECT * FROM gemseconomy_currencies WHERE is_default = 1;");
                    UUID defaultCurrency = null;
                    while(result.next()){
                        defaultCurrency = UUID.fromString(result.getString("uuid"));
                        break;
                    }
                    EconomyMain.consoleMessage("&4Found default currency with UUID &7: "+defaultCurrency);
                    ResultSet moneyResults = connection.createStatement().executeQuery("SELECT * FROM gemseconomy_balances WHERE currency_id = '"+defaultCurrency+"' ;");
                    while(moneyResults.next()){
                        UUID playerUUID = UUID.fromString(moneyResults.getString("account_id"));
                        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(playerUUID);
                        if(offlinePlayer == null)
                            continue;
                        double money = moneyResults.getDouble("balance");

                        if (overwrite == false && EconomyMain.economy.hasAccount(offlinePlayer)) {
                            EconomyMain.consoleMessage("&e"+offlinePlayer.getName()+" &calready has an account. Skipping! &7[Overwrite = false!]");
                        }
                        else if(overwrite == true && EconomyMain.economy.hasAccount(offlinePlayer)){
                            EconomyMain.economy.withdrawPlayer(offlinePlayer,EconomyMain.economy.getBalance(offlinePlayer));
                            EconomyMain.economy.depositPlayer(offlinePlayer,money);
                            EconomyMain.consoleMessage("&e"+offlinePlayer.getName()+" &aconverted successfully with Balance: &b"+money);
                        }
                        else{
                            EconomyMain.economy.createPlayerAccount(offlinePlayer);
                            EconomyMain.economy.depositPlayer(offlinePlayer,money);
                            EconomyMain.consoleMessage("&e"+offlinePlayer.getName()+" &aconverted successfully with Balance: &b"+money);
                        }
                    }
                    break;
                default: break;
            }
        }

        return false;
    }

    @Override
    public boolean sqlConverter(boolean overwrite) throws SQLException {
        return false;
    }
}
