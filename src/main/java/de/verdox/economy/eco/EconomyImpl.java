package de.verdox.economy.eco;

import de.verdox.economy.EconomyMain;
import de.verdox.economy.eco.model.PlayerBalance;
import de.verdox.economy.utils.configuration.ConfigHandler;
import de.verdox.economy.utils.model.Callback;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class EconomyImpl implements Economy {

    public boolean isEnabled() {
        return true;
    }

    public String getName() {
        return "Verdox Economy created by Nightstr3am & Verdox";
    }

    public boolean hasBankSupport() {
        return false;
    }

    public int fractionalDigits() {
        return 2;
    }

    public String format(double v) {
        BigDecimal bd = new BigDecimal(v).setScale(2, RoundingMode.HALF_EVEN);
        return String.valueOf(bd.doubleValue());
    }

    public String currencyNamePlural() {
        return ConfigHandler.getCurrencyName()+"s";
    }

    public String currencyNameSingular() {
        return ConfigHandler.getCurrencyName();
    }

    public boolean hasAccount(String s) {
        return hasAccount(Bukkit.getOfflinePlayer(s));
    }

    public boolean hasAccount(OfflinePlayer offlinePlayer) {
        if(offlinePlayer == null)
            throw new NullPointerException("OfflinePlayer can't be null!");
        if(EconomyMain.dataConnection == null)
            throw new NullPointerException("DataConnection can't be null!");
        PlayerBalance economyResponse = EconomyMain.dataConnection.getUsersBalance(offlinePlayer);
        return economyResponse != null && economyResponse.getUUID().equals(offlinePlayer.getUniqueId().toString());
    }

    public boolean hasAccount(String s, String s1) {
        return hasAccount(Bukkit.getOfflinePlayer(s));
    }

    public boolean hasAccount(OfflinePlayer offlinePlayer, String s) {
        if(offlinePlayer == null)
            throw new NullPointerException("OfflinePlayer can't be null!");
        return hasAccount(offlinePlayer);
    }

    public double getBalance(String s) {
        return getBalance(Bukkit.getOfflinePlayer(s));
    }

    public double getBalance(OfflinePlayer offlinePlayer) {
        if(offlinePlayer == null)
            throw new NullPointerException("OfflinePlayer can't be null!");
        if(EconomyMain.dataConnection == null)
            throw new NullPointerException("DataConnection can't be null!");
        PlayerBalance playerBalance = EconomyMain.dataConnection.getUsersBalance(offlinePlayer);
        if(playerBalance.getBalance()>ConfigHandler.getMaxMoney()){
            setMoney(offlinePlayer,ConfigHandler.getMaxMoney());
            return ConfigHandler.getMaxMoney();
        }
        return playerBalance.getBalance();
    }

    public double getBalance(String s, String s1) {
        return getBalance(s);
    }

    public double getBalance(OfflinePlayer offlinePlayer, String s) {
        return getBalance(offlinePlayer);
    }

    public boolean has(String s, double v) {
        return has(Bukkit.getOfflinePlayer(s),v);
    }

    public boolean has(OfflinePlayer offlinePlayer, double v) {
        return getBalance(offlinePlayer) >= v;
    }

    public boolean has(String s, String s1, double v) {
        return has(s,v);
    }

    public boolean has(OfflinePlayer offlinePlayer, String s, double v) {
        return has(offlinePlayer,v);
    }

    public EconomyResponse withdrawPlayer(String s, double v) {
        return withdrawPlayer(Bukkit.getOfflinePlayer(s),v);
    }

    public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, double v) {
        if(offlinePlayer == null)
            throw new NullPointerException("OfflinePlayer can't be null!");
        if(EconomyMain.dataConnection == null)
            throw new NullPointerException("DataConnection can't be null!");
        EconomyMain.dataConnection.withDrawPlayer(offlinePlayer.getUniqueId().toString(),v);
        return new EconomyResponse(v,getBalance(offlinePlayer), EconomyResponse.ResponseType.SUCCESS,"");
    }

    public EconomyResponse withdrawPlayer(String s, String s1, double v){
        return withdrawPlayer(s,v);
    }

    public EconomyResponse withdrawPlayer(OfflinePlayer offlinePlayer, String s, double v) {
        return withdrawPlayer(offlinePlayer,v);
    }

    public EconomyResponse depositPlayer(String s, double v) {
        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(s);
        if(offlinePlayer == null){
            offlinePlayer = Bukkit.getPlayer(s);
            if(offlinePlayer != null){
                return depositPlayer(offlinePlayer,v);
            }
        }
        return new EconomyResponse(v,0, EconomyResponse.ResponseType.FAILURE,"No player found via the name! (Deprecated)");
    }

    public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, double v) {
        if(offlinePlayer == null)
            throw new NullPointerException("OfflinePlayer can't be null!");
        if(EconomyMain.dataConnection == null)
            throw new NullPointerException("DataConnection can't be null!");
        if(offlinePlayer != null && offlinePlayer.getUniqueId() != null){
            EconomyMain.dataConnection.depositToPlayer(offlinePlayer.getUniqueId().toString(),v);
            return new EconomyResponse(v,getBalance(offlinePlayer), EconomyResponse.ResponseType.SUCCESS,"");
        }
        return new EconomyResponse(v,0, EconomyResponse.ResponseType.FAILURE,"No Offline Player provided!");
    }

    public EconomyResponse depositPlayer(String s, String s1, double v) {
        return depositPlayer(s,v);
    }

    public EconomyResponse depositPlayer(OfflinePlayer offlinePlayer, String s, double v) {
        return depositPlayer(offlinePlayer,v);
    }

    public EconomyResponse createBank(String s, String s1) {
        return null;
    }

    public EconomyResponse createBank(String s, OfflinePlayer offlinePlayer) {
        return null;
    }

    public EconomyResponse deleteBank(String s) {
        return null;
    }

    public EconomyResponse bankBalance(String s) {
        return null;
    }

    public EconomyResponse bankHas(String s, double v) {
        return null;
    }

    public EconomyResponse bankWithdraw(String s, double v) {
        return null;
    }

    public EconomyResponse bankDeposit(String s, double v) {
        return null;
    }

    public EconomyResponse isBankOwner(String s, String s1) {
        return null;
    }

    public EconomyResponse isBankOwner(String s, OfflinePlayer offlinePlayer) {
        return null;
    }

    public EconomyResponse isBankMember(String s, String s1) {
        return null;
    }

    public EconomyResponse isBankMember(String s, OfflinePlayer offlinePlayer) {
        return null;
    }

    public List<String> getBanks() {
        return null;
    }

    public boolean createPlayerAccount(String s) {
        return createPlayerAccount(Bukkit.getOfflinePlayer(s));
    }

    public boolean createPlayerAccount(OfflinePlayer offlinePlayer) {
        if(!hasAccount(offlinePlayer)){
            EconomyMain.dataConnection.createUserInstance(offlinePlayer);
            return true;
        }
        else {
            EconomyMain.dataConnection.updateUserInstance(offlinePlayer);
            return true;
        }
    }

    public boolean createPlayerAccount(String displayName, OfflinePlayer offlinePlayer){
        if(!hasAccount(offlinePlayer)){
            EconomyMain.dataConnection.createUserInstance(offlinePlayer,displayName);
            return true;
        }
        else {
            EconomyMain.dataConnection.updateUserInstance(offlinePlayer);
            return true;
        }
    }

    public boolean createPlayerAccount(String s, String s1) {
        return false;
    }

    public boolean createPlayerAccount(OfflinePlayer offlinePlayer, String s) {
        return false;
    }

    public void getTopPlayers(Callback<List<PlayerBalance>> callback, int page){
        EconomyMain.dataConnection.getTopPlayers(callback, page);
    }

    public boolean setMoney(OfflinePlayer offlinePlayer, double v){
        EconomyMain.dataConnection.setMoney(offlinePlayer.getUniqueId().toString(),v);
        return true;
    }
}
