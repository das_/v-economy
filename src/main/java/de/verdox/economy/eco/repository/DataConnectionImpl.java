package de.verdox.economy.eco.repository;

import de.verdox.economy.EconomyMain;
import de.verdox.economy.eco.model.PlayerBalance;
import de.verdox.economy.utils.configuration.ConfigHandler;
import de.verdox.economy.utils.model.Callback;
import de.verdox.economy.utils.model.ErrorMessage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

public class DataConnectionImpl implements DataConnection {
    private String DB_HOST;
    private String DB_USER;
    private String DB_PW;
    private String DB_DB;
    private String DB_PORT;
    private boolean SSL;
    private Connection connection;
    private Logger logger = Logger.getLogger("[VE-DataConnection]");
    private EconomyMain EconomyMain;
    private ConcurrentHashMap<UUID,Double> balanceCache = new ConcurrentHashMap<>();

    private String tableEconomy = ConfigHandler.getDatabasePrefix()+"_veconomy";
    private String tableLogs = ConfigHandler.getDatabasePrefix()+"_economy_logs";

    public DataConnectionImpl(EconomyMain EconomyMain) {
        this.EconomyMain = EconomyMain;
        this.DB_HOST = ConfigHandler.getString("DB_HOST");
        this.DB_USER = ConfigHandler.getString("DB_USER");
        this.DB_PW = ConfigHandler.getString("DB_PW");
        this.DB_DB = ConfigHandler.getString("DB_DB");
        this.DB_PORT = ConfigHandler.getString("DB_PORT");
        this.SSL = ConfigHandler.useSSL();
        try{
            connect();
        } catch (SQLException e) {
            EconomyMain.consoleMessage("&4Couldn't connect to database&7: &e"+e.getMessage());
            EconomyMain.getServer().getPluginManager().disablePlugin(EconomyMain);
        } catch (ClassNotFoundException e) {
            EconomyMain.consoleMessage("&4Couldn't connect to database&7: &e"+e.getMessage());
            EconomyMain.getServer().getPluginManager().disablePlugin(EconomyMain);
        }

    }

    /***
     * Connect to database
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    private void connect() throws ClassNotFoundException, SQLException {
        if (connection != null && !connection.isClosed()) {
            return;
        }

        synchronized (this) {
            if (connection != null && !connection.isClosed()) {
                return;
            }
            Class.forName("com.mysql.jdbc.Driver");
            if(this.DB_PW == null || this.DB_PW.isEmpty() || this.DB_PW.equals("nopw")){
                EconomyMain.consoleMessage("&eTrying to connect without &aMySQL-Passwort&7!");
                connection = DriverManager.getConnection("jdbc:mysql://" + this.DB_HOST + ":" + this.DB_PORT + "/" + this.DB_DB+"?useSSL="+SSL, this.DB_USER, null);
            }
            else{
                connection = DriverManager.getConnection("jdbc:mysql://" + this.DB_HOST + ":" + this.DB_PORT + "/" + this.DB_DB+"?useSSL="+SSL, this.DB_USER, this.DB_PW);
            }
        }
        createTablesIfNotExist();
    }

    @Override
    public void convertGMoney(Player player, String tablename) {
        runAsync(() -> {
            try{
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT 1 FROM ? LIMIT 1");
                preparedStatement.setString(1,tablename);
                preparedStatement.execute();
                PreparedStatement getUsers = connection.prepareStatement("SELECT * FROM ?");
                getUsers.setString(1,tablename);
                ResultSet users = getUsers.executeQuery();
                if (!users.next()) {
                    player.sendMessage(ChatColor.RED + "No users found to convert in table " + tablename);

                }
                else {
                    do {
                        String possibleUUID = users.getString("player");
                        double money = users.getDouble("money");
                        if(UUID.fromString(possibleUUID).toString().equals(possibleUUID)){
                            //Saved with UUID
                            OfflinePlayer possibleOP = Bukkit.getOfflinePlayer(UUID.fromString(possibleUUID));
                            createUserInstanceFromGMoney(possibleUUID,possibleOP.getName(),money);
                            player.sendMessage(ChatColor.GREEN + "Converted " + possibleOP.getName() + " (Errors may be occured, look in the console logs!)");
                        }
                        else{
                            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(possibleUUID);
                            createUserInstanceFromGMoney(offlinePlayer.getUniqueId().toString(),possibleUUID,money);
                            player.sendMessage(ChatColor.GREEN + "Converted " + offlinePlayer.getName() + " (Errors may be occured, look in the console logs!)");
                        }
                    } while (users.next());
                }
            }
            catch (SQLException e){
                player.sendMessage(ErrorMessage.ERROR.getMsg() + " or table does not exist!");
            }
        });
    }

    /***
     * Create Tables for the plugin
     * @throws SQLException
     */
    private void createTablesIfNotExist() throws SQLException {
        Statement statement = connection.createStatement();

        statement.execute("CREATE TABLE IF NOT EXISTS `"+tableEconomy+"` (" +
                "  `id` int(11) unsigned NOT NULL AUTO_INCREMENT," +
                "  `uuid` varchar(64) NOT NULL DEFAULT ''," +
                "  `balance` double NOT NULL DEFAULT '0'," +
                "  `name` varchar(64) NOT NULL DEFAULT ''," +
                "  PRIMARY KEY (`id`)\n" +
                ") ");

        if(ConfigHandler.useLogs()){
            statement.execute("CREATE TABLE IF NOT EXISTS `"+tableLogs+"` (" +
                    "  `id` int(11) unsigned NOT NULL AUTO_INCREMENT," +
                    "  `senderUUID` varchar(64) NOT NULL DEFAULT ''," +
                    "  `receiverUUID` varchar(64) NOT NULL DEFAULT ''," +
                    "  `senderName` varchar(24) NOT NULL DEFAULT ''," +
                    "  `receiverName` varchar(24) NOT NULL DEFAULT ''," +
                    "  `amount` double NOT NULL DEFAULT '0'," +
                    "  `description` varchar(120) NOT NULL DEFAULT ''," +
                    "  PRIMARY KEY (`id`)\n" +
                    ") ");
        }

        statement.execute("ALTER TABLE `"+tableEconomy+"`" +
                "MODIFY COLUMN `name` varchar(64);");

        try{
            statement.execute("ALTER TABLE veconomy ADD mysqlPrefix IF NOT EXIST varchar(64) NOT NULL DEFAULT '"+ConfigHandler.getDatabasePrefix()+"'");
            convertOldEconomyTable("veconomy");
        }
        catch(SQLException e){}
    }

    private void convertOldEconomyTable(String fromTable) throws SQLException{

        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM "+fromTable);
        ResultSet resultSet = preparedStatement.executeQuery();
        while(resultSet.next()){
            if(resultSet.getString("mysqlPrefix").equals(ConfigHandler.getDatabasePrefix())){
                UUID uuid = UUID.fromString(resultSet.getString("uuid"));
                double balance = resultSet.getDouble("balance");
                String name = resultSet.getString("name");

                PreparedStatement update = connection.prepareStatement("INSERT INTO "+tableEconomy+" (uuid,balance,name) VALUES (?,?,?)");
                update.setString(1,uuid.toString());
                update.setDouble(2,balance);
                update.setString(3,name);
                update.executeUpdate();

                PreparedStatement converted = connection.prepareStatement("UPDATE "+fromTable+" SET mysqlPrefix = ? WHERE uuid = ?");
                converted.setString(1,"already_converted");
                converted.setString(2,uuid.toString());
                converted.executeUpdate();
            }
        }

    }

    private void connectIfConnectionNotActive() throws SQLException {
        if(connection.isClosed()){
            try {
                connect();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void writeLog(CommandSender sender, OfflinePlayer receiver, double amount, String description) {
        if(!ConfigHandler.useLogs())
            return;
        runAsync(() -> {
            try {
                connectIfConnectionNotActive();
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO "+tableLogs+" (senderUUID,receiverUUID,senderName,receiverName,amount,description) VALUES (?,?,?,?,?,?)");
                String senderId = sender.getName();
                if(sender instanceof Player){
                    senderId = ((Player) sender).getUniqueId().toString();
                }
                preparedStatement.setString(1,senderId);
                preparedStatement.setString(2,receiver.getUniqueId().toString());
                preparedStatement.setString(3,sender.getName());
                preparedStatement.setString(4,receiver.getName());
                preparedStatement.setDouble(5,amount);
                preparedStatement.setString(6,description);
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public PlayerBalance getUsersBalance(OfflinePlayer offlinePlayer){
        if(offlinePlayer != null){
            try{
                connectIfConnectionNotActive();
                /*
                                if(balanceCache.containsKey(offlinePlayer.getUniqueId())) {
                    if (balanceCache.get(offlinePlayer.getUniqueId()) >= 0) {
                        return new PlayerBalance(offlinePlayer.getUniqueId().toString(), balanceCache.get(offlinePlayer.getUniqueId()), offlinePlayer.getName());
                    }
                }
                 */

                PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM "+tableEconomy+" WHERE uuid = ?");
                preparedStatement.setString(1,offlinePlayer.getUniqueId().toString());
                ResultSet resultSet = preparedStatement.executeQuery();
                if(resultSet.next()){
                    PlayerBalance playerBalance = new PlayerBalance(offlinePlayer.getUniqueId().toString(),resultSet.getDouble("balance"),resultSet.getString("name"));
                    balanceCache.put(offlinePlayer.getUniqueId(),playerBalance.getBalance());
                    return playerBalance;
                }
            }
            catch (SQLException e){
                e.printStackTrace();
            }
            return new PlayerBalance("",0.,"no resultset obtained");
        }
        return new PlayerBalance("",0.,"no offline player sent!");
    }

    public void createUserInstance(OfflinePlayer offlinePlayer) {
        if(offlinePlayer != null){
            runAsync(()-> {
                try{
                    connectIfConnectionNotActive();
                    PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO "+tableEconomy+" (uuid,balance,name) VALUES (?,?,?)");
                    preparedStatement.setString(1,offlinePlayer.getUniqueId().toString());
                    preparedStatement.setDouble(2,ConfigHandler.getStartMoney());
                    if(offlinePlayer.getName() == null){
                        preparedStatement.setString(3,"unknown");
                    }
                    else
                        preparedStatement.setString(3,offlinePlayer.getName());

                    preparedStatement.executeUpdate();
                    if(offlinePlayer instanceof Player){
                        addPlayerToBalanceCache((Player)offlinePlayer);
                    }
                }
                catch (SQLException e){
                    e.printStackTrace();
                }
            });
        }
    }

    public void createUserInstance(OfflinePlayer offlinePlayer, String userName) {
        if(offlinePlayer != null){
            runAsync(()-> {
                try{
                    connectIfConnectionNotActive();
                    PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO "+tableEconomy+" (uuid,balance,name) VALUES (?,?,?)");
                    preparedStatement.setString(1,offlinePlayer.getUniqueId().toString());
                    preparedStatement.setDouble(2,ConfigHandler.getStartMoney());
                    if(offlinePlayer.getName() == null){
                        preparedStatement.setString(3,userName);
                    }
                    else
                        preparedStatement.setString(3,offlinePlayer.getName());

                    preparedStatement.executeUpdate();
                    if(offlinePlayer instanceof Player){
                        addPlayerToBalanceCache((Player)offlinePlayer);
                    }
                }
                catch (SQLException e){
                    System.out.println(offlinePlayer.getUniqueId().toString());
                    System.out.println(userName);
                    e.printStackTrace();
                }
            });
        }
    }

    private void createUserInstanceFromGMoney(String uuid, String name, double balance) {
            runAsync(()-> {
                try{
                    connectIfConnectionNotActive();
                    PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO "+tableEconomy+" (uuid,balance,name) VALUES (?,?,?)");
                    preparedStatement.setString(1,uuid);
                    preparedStatement.setDouble(2,balance);
                    preparedStatement.setString(3,name);
                    preparedStatement.executeUpdate();
                }
                catch (SQLException e){
                    e.printStackTrace();
                }
            });
    }

    public void withDrawPlayer(String uuid, double amount) {
        runAsync(() -> {
            try{
                connectIfConnectionNotActive();
                PreparedStatement preparedStatement = connection.prepareStatement("UPDATE "+tableEconomy+" SET balance = balance - ? WHERE uuid = ?");
                preparedStatement.setDouble(1,amount);
                preparedStatement.setString(2,uuid);
                preparedStatement.executeUpdate();
                balanceCache.put(UUID.fromString(uuid),balanceCache.get(UUID.fromString(uuid))-amount);
            }
            catch (SQLException e){
                e.printStackTrace();
            }
        });
    }

    public void depositToPlayer(String uuid, double amount) {
        runAsync(() -> {
            try{
                connectIfConnectionNotActive();
                PreparedStatement preparedStatement = connection.prepareStatement("UPDATE "+tableEconomy+" SET balance = balance + ? WHERE uuid = ?");
                preparedStatement.setDouble(1,amount);
                preparedStatement.setString(2,uuid);
                preparedStatement.executeUpdate();
                if(uuid != null && balanceCache.get(UUID.fromString(uuid)) != null){
                    balanceCache.put(UUID.fromString(uuid),balanceCache.get(UUID.fromString(uuid))+amount);
                }
            }
            catch (SQLException e){
                e.printStackTrace();
            }
        });
    }

    @Override
    public void setMoney(String uuid, double amount) {
        runAsync(() -> {
            try{
                connectIfConnectionNotActive();
                PreparedStatement preparedStatement = connection.prepareStatement("UPDATE "+tableEconomy+" SET balance = ? WHERE uuid = ?");
                preparedStatement.setDouble(1,amount);
                preparedStatement.setString(2,uuid);
                preparedStatement.executeUpdate();
                balanceCache.put(UUID.fromString(uuid),amount);
            }
            catch (SQLException e){
                e.printStackTrace();
            }
        });
    }

    @Override
    public void updateUserInstance(OfflinePlayer offlinePlayer) {
        this.addPlayerToBalanceCache((Player)offlinePlayer);
        runAsync(() -> {
            try{
                connectIfConnectionNotActive();
                PreparedStatement preparedStatement = connection.prepareStatement("UPDATE "+tableEconomy+" SET name = ? WHERE uuid = ?");
                preparedStatement.setString(1,offlinePlayer.getName());
                preparedStatement.setString(2,offlinePlayer.getUniqueId().toString());
                preparedStatement.executeUpdate();
            }
            catch (SQLException e){
                e.printStackTrace();
            }
        });
    }

    @Override
    public void getTopPlayers(Callback<List<PlayerBalance>> callback, int page) {
        runAsync(() -> {
            try{
                connectIfConnectionNotActive();
                PreparedStatement preparedStatement = connection.prepareStatement("SELECT uuid,balance,name FROM "+tableEconomy+" ORDER BY balance DESC limit " + page *10);
                ResultSet resultSet = preparedStatement.executeQuery();
                List<PlayerBalance> playerBalances = new ArrayList<>();
                while(resultSet.next()){
                    playerBalances.add(new PlayerBalance(resultSet.getString("uuid"),resultSet.getDouble("balance"),resultSet.getString("name")));
                }
                if(playerBalances.size() > 10){
                    callback.taskDone(playerBalances.subList((page*10)-10,playerBalances.size()-1));
                }
                else{
                    callback.taskDone(playerBalances);
                }

            }
            catch (SQLException e){
                e.printStackTrace();
            }
        });
    }

    @Override
    public void addPlayerToBalanceCache(Player player) {
        balanceCache.put(player.getUniqueId(),-1.0);
    }

    @Override
    public void removePlayerFromBalanceCache(Player player) {
        balanceCache.remove(player.getUniqueId());
    }

    private void runAsync(final Runnable runnable){
        BukkitRunnable r = new BukkitRunnable() {
            public void run() {
                runnable.run();
            }
        };
        r.runTaskAsynchronously(EconomyMain);
    }
}
