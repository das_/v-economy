package de.verdox.economy.eco.repository;

import de.verdox.economy.eco.model.PlayerBalance;
import de.verdox.economy.utils.model.Callback;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public interface DataConnection {
    PlayerBalance getUsersBalance(OfflinePlayer offlinePlayer);
    void createUserInstance(OfflinePlayer offlinePlayer);
    void withDrawPlayer(String uuid, double amount);
    void depositToPlayer(String uuid, double amount);
    void getTopPlayers(Callback<List<PlayerBalance>> callback, int page);
    void setMoney(String uuid,double amount);
    void updateUserInstance(OfflinePlayer offlinePlayer);
    void addPlayerToBalanceCache(Player player);
    void removePlayerFromBalanceCache(Player player);
    void writeLog(CommandSender sender, OfflinePlayer receiver, double amount, String description);
    void convertGMoney(Player player, String tablename);
}
