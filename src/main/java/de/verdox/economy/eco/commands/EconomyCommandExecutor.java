package de.verdox.economy.eco.commands;

import de.verdox.economy.EconomyMain;
import de.verdox.economy.converter.Converter;
import de.verdox.economy.converter.EssentialsConverter;
import de.verdox.economy.converter.GemsEconomyConverter;
import de.verdox.economy.eco.EconomyImpl;
import de.verdox.economy.utils.Util;
import de.verdox.economy.utils.configuration.ConfigHandler;
import de.verdox.economy.utils.model.ErrorMessage;
import de.verdox.economy.utils.model.SuccessMessage;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.sql.SQLException;

public class EconomyCommandExecutor implements CommandExecutor {

    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        final CommandSender sender = commandSender;
        if(command.getLabel().equals("money")){
            if(!(sender instanceof Player))
                return false;
            if(strings.length > 0 && (sender.hasPermission("economy.admin") || sender.isOp())){
                Player player = Bukkit.getPlayer(strings[0]);
                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(strings[0]);
                if(player != null){
                    sender.sendMessage(SuccessMessage.PLAYER_OWNS.getMsg(EconomyMain.economy.getBalance(player),player.getName()));
                }
                else{
                    sender.sendMessage(ErrorMessage.PLAYER_DOES_NOT_EXIST.getMsg());
                }
            }
            else{
                sender.sendMessage(SuccessMessage.MONEY_OWNED.getMsg(EconomyMain.economy.getBalance((Player)sender),sender.getName()));
            }
            return true;
        }
        /*
        /economyimport <Converter> <yml/sqlite/mysql> <overwrite>
         */
        else if(command.getLabel().equalsIgnoreCase("economyimport")){
            if(sender.hasPermission("economy.admin") || sender.isOp()){
                if(strings.length == 3){
                    String converterName = strings[0];
                    String conversionType = strings[1];
                    boolean overwrite = Boolean.parseBoolean(strings[2]);
                    if(converterName.equalsIgnoreCase(Converter.ConverterType.Essentials.name())){
                        if(conversionType.equalsIgnoreCase("yml")){
                            sender.sendMessage(SuccessMessage.CONVERSION_START.getMsg());
                            new EssentialsConverter().ymlConvert(overwrite);
                            return true;
                        }
                        else if(conversionType.equalsIgnoreCase("sqlite")){
                            sender.sendMessage(ErrorMessage.No_SQLite.getMsg());
                            return true;
                        }
                        else if(conversionType.equalsIgnoreCase("mysql")){
                            sender.sendMessage(ErrorMessage.No_MySQL.getMsg());
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    else if(converterName.equalsIgnoreCase(Converter.ConverterType.GemsEconomy.name())){
                        if(conversionType.equalsIgnoreCase("yml")){
                            sender.sendMessage(SuccessMessage.CONVERSION_START.getMsg());
                            new GemsEconomyConverter().ymlConvert(overwrite);
                            return true;
                        }
                        else if(conversionType.equalsIgnoreCase("sqlite")){
                            sender.sendMessage(SuccessMessage.CONVERSION_START.getMsg());
                            try {
                                new GemsEconomyConverter().sqLiteConvert(overwrite);
                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                            return true;
                        }
                        else if(conversionType.equalsIgnoreCase("mysql")){
                            sender.sendMessage(ErrorMessage.NOT_YET_IMPLEMENTED.getMsg());
                            return false;
                        }
                    }
                    else {
                        sender.sendMessage(ErrorMessage.Enter_ValidImport.getMsg());
                        return false;
                    }
                }
            }
        }
        else if(command.getLabel().equals("pay")){
            if(strings.length > 1){
                String playerName = strings[0];
                double amountTmp;
                try{
                    amountTmp = Double.parseDouble(strings[1]);
                }
                catch(NumberFormatException e){
                    amountTmp = -1;
                }
                final double amount = amountTmp;
                final OfflinePlayer player = (OfflinePlayer)sender;
                if(amount > 0 && EconomyMain.economy.has(player,amount)){
                    OfflinePlayer playerTo = Bukkit.getPlayer(playerName);
                    if(playerTo == null){
                        playerTo = Bukkit.getOfflinePlayer(playerName);
                    }
                    if(amount>ConfigHandler.getMaxPayAmount()){
                        sender.sendMessage(ErrorMessage.Max_Pay_Reached.getMsg());
                        return false;
                    }
                    if(EconomyMain.economy.hasAccount(playerTo)){
                        EconomyMain.economy.withdrawPlayer(player,amount);
                        EconomyMain.economy.depositPlayer(playerTo,amount);
                        EconomyMain.dataConnection.writeLog(sender,playerTo,amount,"InGame - /pay");
                        sender.sendMessage(SuccessMessage.SENT_MONEY_TO_PLAYER.getMsg(amount,playerTo.getName()));
                        if(playerTo.isOnline()){
                            ((Player)playerTo).sendMessage(SuccessMessage.RECEIVED_MONEY_FROM_PLAYER.getMsg(amount,player.getName()));
                        }
                        return true;
                    }
                    else {
                        sender.sendMessage(ErrorMessage.PLAYER_DOES_NOT_EXIST.getMsg());
                    }
                }
                else{
                    sender.sendMessage(ErrorMessage.NOT_A_VALID_AMOUNT.getMsg());
                }
            }
        }
        else if(command.getLabel().equals("baltop")){
            int page = 1;
            if(strings.length > 0){
                try{
                    page = Integer.parseInt(strings[0]);
                }
                catch (Exception e){

                }
            }
            final int finalPage = page > 0 ? page : 1;
            ((EconomyImpl)EconomyMain.economy).getTopPlayers(playerBalances -> {
                if(playerBalances != null && !playerBalances.isEmpty()){
                    sender.sendMessage(SuccessMessage.TOP_PLAYERS.getMsg());
                    final int[] counter = {((finalPage*10)-9)};
                    playerBalances.forEach(p -> {
                        sender.sendMessage(Util.buildColoredMessage(counter[0],". ",p.getName(),ChatColor.GREEN," ",(Math.round(p.getBalance()*100)/100)+ ConfigHandler.getCurrencyType()));
                        counter[0]++;
                    });
                }
            },page);
            return true;
        }
        else if(command.getLabel().equals("reloadeconomy")){
            if(sender.hasPermission("economy.admin") || sender.isOp() || sender instanceof ConsoleCommandSender){
                ConfigHandler.reloadConfig();
                sender.sendMessage(SuccessMessage.SUCCESS.getMsg());
            }
            else sender.sendMessage(ErrorMessage.LACK_OF_PERMISSIONS.getMsg());
        }
        else if(command.getLabel().equals("eco")){
            if(sender.hasPermission("economy.admin") || sender.isOp()){
                if(strings.length == 1){
                    if(strings[0].equalsIgnoreCase("help")){

                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',"     &eV-Economy Help"));
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',"     &7/&amoney"));
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',"     &7/&apay <Player> <Amount>"));
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',"     &7/&abaltop <page>"));
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',"     &7/&aeco <set|give|take|reset> <Player> <Amount>"));
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',"     &7/&areloadeconomy"));
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&',"     &7/&aecomomyimport <Plugin> <overwrite accounts (true|false)>"));
                        return true;
                    }
                }
                else if(strings.length > 2){
                    final OfflinePlayer playerTo = Bukkit.getOfflinePlayer(strings[1]);
                    if(EconomyMain.economy.hasAccount(playerTo)){
                        try{
                            final double amount = Double.parseDouble(strings[2]);
                            switch(strings[0]){
                                case "set": {
                                    if(((EconomyImpl)EconomyMain.economy).setMoney(playerTo,amount)){
                                        sender.sendMessage(SuccessMessage.ADMIN_SET_MONEY.getMsg(amount,playerTo.getName()));
                                        EconomyMain.dataConnection.writeLog(sender,playerTo,amount,"InGame /eco set");
                                    }
                                    else{
                                        sender.sendMessage(ErrorMessage.ERROR.getMsg());
                                    }
                                    return true;
                                }
                                case "give": {
                                    if(EconomyMain.economy.depositPlayer(playerTo,amount).type.equals(EconomyResponse.ResponseType.SUCCESS)){
                                        sender.sendMessage(SuccessMessage.ADMIN_GIVE_MONEY.getMsg(amount,playerTo.getName()));
                                        EconomyMain.dataConnection.writeLog(sender,playerTo,amount,"InGame /eco give");
                                    }
                                    else{
                                        sender.sendMessage(ErrorMessage.ERROR.getMsg());
                                    }
                                    return true;
                                }
                                case "take": {
                                    if(EconomyMain.economy.withdrawPlayer(playerTo,amount).type.equals(EconomyResponse.ResponseType.SUCCESS)){
                                        sender.sendMessage(SuccessMessage.ADMIN_TAKE_MONEY.getMsg(amount,playerTo.getName()));
                                        EconomyMain.dataConnection.writeLog(sender,playerTo,-amount,"InGame /eco take");
                                    }
                                    else{
                                        sender.sendMessage(ErrorMessage.ERROR.getMsg());
                                    }
                                    return true;
                                }
                            }
                        }
                        catch (NumberFormatException e){
                            sender.sendMessage(ErrorMessage.NOT_A_VALID_AMOUNT.getMsg());
                        }
                    }
                    else{
                        sender.sendMessage(ErrorMessage.PLAYER_DOES_NOT_EXIST.getMsg());
                    }
                }
                else if(strings.length == 2){
                    final OfflinePlayer playerTo = Bukkit.getOfflinePlayer(strings[1]);
                    if(strings[0].equalsIgnoreCase("reset")){
                        EconomyMain.economy.setMoney(playerTo,ConfigHandler.getStartMoney());
                        sender.sendMessage(SuccessMessage.ADMIN_RESET.getMsg());
                        return true;
                    }
                }
            }
            else{
                sender.sendMessage(ErrorMessage.LACK_OF_PERMISSIONS.getMsg());
                return true;
            }
        }
        return false;
    }
}
