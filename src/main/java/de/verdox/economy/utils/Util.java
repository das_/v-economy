package de.verdox.economy.utils;

public class Util {
    public static String buildColoredMessage(Object... args){
        StringBuilder stringBuilder = new StringBuilder();
        for(Object o : args){
            stringBuilder.append(o);
        }
        return stringBuilder.toString();
    }
}
