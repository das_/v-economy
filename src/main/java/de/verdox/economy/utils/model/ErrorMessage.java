package de.verdox.economy.utils.model;

import de.verdox.economy.utils.configuration.ConfigHandler;

public enum ErrorMessage {
    ERROR("&cSomething went wrong!"),
    LACK_OF_PERMISSIONS("&cYou lack of permissions!"),
    NOT_ENOUGH_MONEY("&cYou do not have enough money!"),
    PLAYER_DOES_NOT_EXIST("&cThe player does not exist!"),
    NOT_A_VALID_AMOUNT("&cNo valid amount!"),
    Enter_ValidImport("&cPlease provide a valid importer: Essentials / more coming soon"),
    No_YML("&4This Economy Plugin does not support Yml savings!"),
    No_SQLite("&4This Economy Plugin does not support SQLite savings!"),
    No_MySQL("&4This Economy Plugin does not support MySQL savings!"),
    NOT_YET_IMPLEMENTED("&4Not yet implemented!"),
    Max_Pay_Reached("&4You can't pay that much money&7!")
    ;

    private String defaultMsg;
    ErrorMessage(String def){
        this.defaultMsg = def;
    }

    public String getMsg(){
        return ConfigHandler.getErrorMessage(this);
    }

    public String getDefaultMsg(){
        return defaultMsg;
    }
}
