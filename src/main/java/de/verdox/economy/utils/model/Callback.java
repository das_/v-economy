package de.verdox.economy.utils.model;

public interface Callback<T> {
    void taskDone(T t);
}
